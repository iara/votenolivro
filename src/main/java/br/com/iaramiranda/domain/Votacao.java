package br.com.iaramiranda.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table
public class Votacao  implements java.io.Serializable {

	@Id
	@GeneratedValue
     private Integer id;
     private Livro livro;
     private Usuario usuario;
     private Integer quatidadeVoto;

    public Votacao() {
    }

    public Votacao(Livro livro, Usuario usuario, Integer quatidadeVoto) {
       this.livro = livro;
       this.usuario = usuario;
       this.quatidadeVoto = quatidadeVoto;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Livro getLivro() {
        return this.livro;
    }
    
    public void setLivro(Livro livro) {
        this.livro = livro;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public Integer getQuatidadeVoto() {
        return this.quatidadeVoto;
    }
    
    public void setQuatidadeVoto(Integer quatidadeVoto) {
        this.quatidadeVoto = quatidadeVoto;
    }




}


