package br.com.iaramiranda.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Usuario  implements java.io.Serializable {

     private Integer id;
     private String email;
     private String name;

    public Usuario(String email) {
        this.email = email;
    }
    public Usuario(String email, String name) {
       this.email = email;
       this.name = name;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}


