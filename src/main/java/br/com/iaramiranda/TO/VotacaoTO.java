package br.com.iaramiranda.TO;

public class VotacaoTO {
    
     private Integer id;
     private String titulo;
     private Integer quantidadeVotada;
     private String nomeDoUsuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getQuantidadeVotada() {
        return quantidadeVotada;
    }

    public void setQuantidadeVotada(Integer quantidadeVotada) {
        this.quantidadeVotada = quantidadeVotada;
    }

    public String getNomeDoUsuario() {
        return nomeDoUsuario;
    }

    public void setNomeDoUsuario(String nomeDoUsuario) {
        this.nomeDoUsuario = nomeDoUsuario;
    }

   
    
}
