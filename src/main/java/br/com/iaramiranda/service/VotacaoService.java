package br.com.iaramiranda.service;

import br.com.iaramiranda.DAO.LivroDao;
import br.com.iaramiranda.DAO.VotacaoDao;
import br.com.iaramiranda.TO.LivroTO;
import br.com.iaramiranda.TO.VotacaoTO;
import br.com.iaramiranda.domain.Livro;
import br.com.iaramiranda.domain.Votacao;
import java.util.ArrayList;
import java.util.List;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VotacaoService {

    @Autowired
    private LivroDao livroDao;

        @Autowired
    private VotacaoDao votacaoDao;

        
    final private Integer NUM_COMBINACOES = 2;

    public List<LivroTO> obterCombinacao() {
        List<Livro> livros = new ArrayList<Livro>();
        List<LivroTO> livroTOs = new ArrayList<LivroTO>();
        Generator<Livro> combinacaoDeLivros = this.obterParesDeLivro();

        for (ICombinatoricsVector<Livro> combination : combinacaoDeLivros) {
            livros.addAll(combination.getVector());
            break;
        }

        for (Livro livro : livros) {
            LivroTO livroTO = new LivroTO();
            livroTO.setAutor(livro.getAutor());
            livroTO.setTitulo(livro.getTitulo());
            livroTO.setPath(livro.getPath());
            livroTO.setId(livro.getId());

            livroTOs.add(livroTO);
        }
        return livroTOs;
    }

    private Generator<Livro> obterParesDeLivro() {
        ICombinatoricsVector<Livro> vetorInicial = Factory.createVector(livroDao.listar());
        return Factory.createSimpleCombinationGenerator(vetorInicial, NUM_COMBINACOES);
    }
    
    public List<VotacaoTO> obterRankingGeral() {
        List<VotacaoTO> votacaoTOs = new ArrayList<VotacaoTO>();
        for (Votacao voto : votacaoDao.listar()) {
            VotacaoTO votoTO = new VotacaoTO();
            votoTO.setId(voto.getId());
            votoTO.setNomeDoUsuario(voto.getUsuario().getName());
            votoTO.setQuantidadeVotada(voto.getQuatidadeVoto());
            votoTO.setTitulo(voto.getLivro().getTitulo());
            votacaoTOs.add(votoTO);
        }
        return votacaoTOs;
    }
}
