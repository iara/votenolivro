package br.com.iaramiranda.controllers;

import br.com.iaramiranda.domain.Usuario;
import br.com.iaramiranda.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Usuario")
public class UsuarioController {
    
    @Autowired
    private UsuarioService service;

    @ResponseBody
    @RequestMapping(value = "/cadastrar")
    public ModelAndView index() {
           ModelAndView model = new ModelAndView("Usuario/cadastrar");
           return model;
    }

    @ResponseBody
    @RequestMapping(value = "/salvar")
    public void salvar(Usuario usuario){
          service.salvar(usuario);
    }

}
