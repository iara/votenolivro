package br.com.iaramiranda.controllers;

import br.com.iaramiranda.TO.LivroTO;
import br.com.iaramiranda.domain.Votacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import br.com.iaramiranda.service.VotacaoService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

@Controller @RequestMapping("/") public class VotacaoController {

    @Autowired private VotacaoService service;

    @ResponseBody @RequestMapping(value = "/")
    public ModelAndView index(@CookieValue(value = "contador", defaultValue = "0") Integer contador,
        HttpServletResponse response, HttpServletRequest  request) throws Exception {
        ModelAndView model = new ModelAndView("votacao/index");

        contador++;
        Cookie cookie = new Cookie("contador", contador.toString());
        response.addCookie(cookie);

        model.addObject("contador", contador);
        model.addObject("contador2", request.getRemoteAddr());
        
        model.addObject("livros", service.obterCombinacao());
        return model;
    }

    @RequestMapping(value = "/votar/{idLivroVotado}", method = RequestMethod.GET)
    public void votar(@PathVariable Integer idLivroVotado) throws Exception {
        service.obterCombinacao();
    }

    @ResponseBody @RequestMapping(value = "/ranking/{usuarioId}", method = RequestMethod.GET)
    public ModelAndView ranking(@PathVariable Integer usuarioId) throws Exception {
        ModelAndView model = new ModelAndView("votacao/ranking");
        model.addObject("rankingGeral", service.obterRankingGeral());
        return model;
    }
}
