var iaramiranda = iaramiranda || {};

iaramiranda.votacao = {
    raiz : "",
	init : function(){
		this.bind();
	},
	bind : function(){
        
        $("body")
            .on('click', '.voto', iaramiranda.votacao.votar)
	},
    votar: function(){
        $.ajax({
            type : 'POST',
            url :  'index/votar',
            data : $("#formIncluir").serialize()
        }).done(function() {
            $("#modalEditarHistoricoLalur").dialog("close");
            setSuccess("Historico LALUR salvo com sucesso!", 5);
            bluesoft.historicoLalur.buscar();
        }).fail(function(){
            $("#modalEditarHistoricoLalur").dialog("close");
            addError("Erro ao salvar historico LALUR.", 5);
        }).always(function(){
            getAguardeTag().hide();
        });
    }
};